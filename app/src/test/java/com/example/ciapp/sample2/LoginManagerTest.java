package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Password");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("Testuser1", "Password");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Password"));
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginWrongPassword() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("iniad", "password");
    }

    @Test(expected = ValidateFailedException.class)
    public void testRegisterWrongUsername() throws ValidateFailedException {
        loginManager.register("testuser2", "Password");
    }

    @Test(expected = ValidateFailedException.class)
    public void testRegisterWrongPassword() throws ValidateFailedException {
        loginManager.register("Testuser2", "password");
    }
}